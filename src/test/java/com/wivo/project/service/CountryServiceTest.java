package com.wivo.project.service;

import com.wivo.project.ProjectApplication;
import com.wivo.project.model.Country;
import com.wivo.project.resource.response.CountryRunwayResponse;
import com.wivo.project.resource.response.CountryTopResponse;
import com.wivo.project.util.Parametros;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Profile;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = {ProjectApplication.class})
@Profile("test")
public class CountryServiceTest {

    @Autowired
    CountryService countryService;

    @Test
    public void findByType(){
        Country country = countryService.findByType(Parametros.COUNTRY_CODE,"VE");
        System.out.println("Cantidad = "+ country.getName());
        assertEquals("Venezuela",country.getName());
    }

    @Test
    public void top10Most(){
        List<CountryTopResponse> list =  countryService.top10Most();
        list.forEach(item->{
            System.out.println("Resultado = "+item.toString());
        });
        assertNotNull(list);
    }

    @Test
    public void top10Less(){
        List<CountryTopResponse> list = countryService.top10Less();
        list.forEach(item->{
            System.out.println("Resultado = "+item.toString());
        });
        assertNotNull(list);
    }

    @Test
    public void countryRunway(){
        List<CountryRunwayResponse> list = countryService.countryRunway();
        list.forEach(item->{
            System.out.println("Resultado = "+item.toString());
        });
        assertNotNull(list);
    }

}
