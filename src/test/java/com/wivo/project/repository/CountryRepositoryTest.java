package com.wivo.project.repository;

import com.wivo.project.ProjectApplication;
import com.wivo.project.model.Country;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Profile;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = {ProjectApplication.class})
@Profile("test")
public class CountryRepositoryTest {


    @Autowired
    private CountryRepository countryRepository;

    @Test
    public void findAll(){
        Long cantidad = countryRepository.count();
        System.out.println("Cantidad = "+ cantidad);
    }

    @Test
    public void ejemplo(){
        Country country = countryRepository.findByCode("VE");
        System.out.println("Cantidad = "+ country.getName());
        assertEquals("Venezuela",country.getName());
    }

}
