package com.wivo.project.resource;

import com.wivo.project.ProjectApplication;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Profile;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = {ProjectApplication.class})
@Profile("test")
public class ReportResourceTest {

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Before
    public void setUp() throws Exception {
        mockMvc = MockMvcBuilders
                .webAppContextSetup(webApplicationContext)
                .build();
    }


    @Test
    public void paisesMayorNumeroAeropuertos(){

        try {
            mockMvc.perform(
                    get("/rest/report/paises-mayor-numero-aeropuertos")
                            .contentType(MediaType.APPLICATION_JSON)
                            .accept(MediaType.APPLICATION_JSON)
            ).andDo(print());

        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    @Test
    public void paisesMenorNumeroAeropuertos(){

        try {
            mockMvc.perform(
                    get("/rest/report/paises-menor-numero-aeropuertos")
                            .contentType(MediaType.APPLICATION_JSON)
                            .accept(MediaType.APPLICATION_JSON)
            ).andDo(print());

        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    @Test
    public void tipoPistarPorPais(){

        try {
            mockMvc.perform(
                    get("/rest/report/tipo-pista-por-pais")
                            .contentType(MediaType.APPLICATION_JSON)
                            .accept(MediaType.APPLICATION_JSON)
            ).andDo(print());

        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    @Test
    public void tiposDePistasMasComunes(){

        try {
            mockMvc.perform(
                    get("/rest/report/tipo-pistas-mas-comunes")
                            .contentType(MediaType.APPLICATION_JSON)
                            .accept(MediaType.APPLICATION_JSON)
            ).andDo(print());

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
