ALTER DATABASE `prueba_bd` CHARACTER SET utf8 COLLATE utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `prueba_bd`.`airports` (
  `id` int(11) NOT NULL,
  `ident` varchar(10) DEFAULT NULL,
  `airport_type` varchar(30) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `latitud_deg` varchar(50) DEFAULT NULL,
  `longitud_deg` varchar(50) DEFAULT NULL,
  `elevation_ft` varchar(10) DEFAULT NULL,
  `continent` char(2) DEFAULT NULL,
  `iso_country` char(2) DEFAULT NULL,
  `iso_region` varchar(10) DEFAULT NULL,
  `municipality` varchar(100) DEFAULT NULL,
  `scheduled_service` enum('yes','no') DEFAULT NULL,
  `gps_code` varchar(5) DEFAULT NULL,
  `iata_code` varchar(3) DEFAULT NULL,
  `local_code` varchar(5) DEFAULT NULL,
  `home_link` varchar(255) DEFAULT NULL,
  `wikipedia_link` varchar(255) DEFAULT NULL,
  `keywords` varchar(255) DEFAULT NULL,
  UNIQUE KEY `idx_unique_id` (`id`),
  KEY `idx_iso_country` (`iso_country`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE IF NOT EXISTS `prueba_bd`.`countries` (
  `id` int(11) NOT NULL,
  `iso_country` char(2) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `continent` char(2) DEFAULT NULL,
  `wikipedia_link` varchar(255) DEFAULT NULL,
  `keywords` varchar(255) DEFAULT NULL,
  UNIQUE KEY `idx_unique_id` (`id`),
  UNIQUE KEY `idx_iso_country` (`iso_country`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE IF NOT EXISTS `prueba_bd`.`runways` (
  `id` int(11) NOT NULL,
  `airport_ref` int(11) DEFAULT NULL,
  `airport_ident` varchar(10) DEFAULT NULL,
  `length_ft` varchar(10) DEFAULT NULL,
  `width_ft` varchar(10) DEFAULT NULL,
  `surface` varchar(100) DEFAULT NULL,
  `lighted` int(1) DEFAULT NULL,
  `closed` int(1) DEFAULT NULL,
  `le_ident` varchar(10) DEFAULT NULL,
  `le_latitude_deg` varchar(10) DEFAULT NULL,
  `le_longitude_deg` varchar(10) DEFAULT NULL,
  `le_elevation_ft` varchar(10) DEFAULT NULL,
  `le_heading_degT` varchar(100) DEFAULT NULL,
  `le_displaced_threshold_ft` varchar(100) DEFAULT NULL,
  `he_ident` varchar(100) DEFAULT NULL,
  `he_latitude_deg` varchar(100) DEFAULT NULL,
  `he_longitude_deg` varchar(255) DEFAULT NULL,
  `he_elevation_ft` varchar(100) DEFAULT NULL,
  `he_heading_degT` varchar(100) DEFAULT NULL,
  `he_displaced_threshold_ft` varchar(100) DEFAULT NULL,
  UNIQUE KEY `idx_unique_id` (`id`),
  KEY `idx_le_ident` (`le_ident`),
  KEY `idx_airport_ref` (`airport_ref`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



ALTER TABLE `prueba_bd`.`airports`
ADD CONSTRAINT `idx_fk_iso_country` FOREIGN KEY (`iso_country`) REFERENCES `countries` (`iso_country`);


ALTER TABLE `prueba_bd`.`runways`
ADD CONSTRAINT `idx_fk_airport_ref` FOREIGN KEY (`airport_ref`) REFERENCES `airports` (`id`);