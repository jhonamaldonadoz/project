package com.wivo.project.service;

import com.wivo.project.model.Country;
import com.wivo.project.resource.response.CountryRunwayResponse;
import com.wivo.project.resource.response.CountryTopResponse;

import java.util.List;

public interface CountryService {

    Country findByType(final String type, final String search);

    List<CountryTopResponse> top10Most();

    List<CountryTopResponse> top10Less();

    List<CountryRunwayResponse> countryRunway();

    Long count();

}
