package com.wivo.project.service;

import com.wivo.project.resource.response.RunwayMostCommonResponse;

import java.util.List;

public interface RunwayService {

    List<RunwayMostCommonResponse> runwayMostCommon();

    Long count();

}
