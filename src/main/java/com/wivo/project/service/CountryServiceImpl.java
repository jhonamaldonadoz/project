package com.wivo.project.service;

import com.wivo.project.model.Country;
import com.wivo.project.repository.CountryRepository;
import com.wivo.project.resource.response.CountryRunwayResponse;
import com.wivo.project.resource.response.CountryTopResponse;
import com.wivo.project.util.Parametros;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
@Transactional
public class CountryServiceImpl implements CountryService {

    @Autowired
    CountryRepository countryRepository;


    @Override
    public Country findByType(String type, String search) {

        Country country = null;

        if(type.equals(Parametros.COUNTRY_CODE)) {
            country = countryRepository.findByCode(search);
        } else if(type.equals(Parametros.COUNTRY_NAME)){
            country = countryRepository.findByName(search);
        }

        return country;

    }


    @Override
    public List<CountryTopResponse> top10Most() {

        List<CountryTopResponse> countryTopResponse = new ArrayList<>();
        List<Object[]> countryList = countryRepository.top10More();

        countryList.forEach(item->{

            CountryTopResponse countryTopResponse1 = new CountryTopResponse();

            countryTopResponse1
                    .nameCountry(item[1].toString())
                    .count(Integer.valueOf(item[2].toString()));

            countryTopResponse.add(countryTopResponse1);

        });

        return countryTopResponse;

    }



    @Override
    public List<CountryTopResponse> top10Less() {

        List<CountryTopResponse> countryTopResponse = new ArrayList<>();
        List<Object[]> countryList = countryRepository.top10Less();

        countryList.forEach(item->{

            CountryTopResponse countryTopResponse1 = new CountryTopResponse();

            countryTopResponse1
                    .nameCountry(item[1].toString())
                    .count(Integer.valueOf(item[2].toString()));

            countryTopResponse.add(countryTopResponse1);

        });

        return countryTopResponse;

    }


    @Override
    public List<CountryRunwayResponse> countryRunway() {

        List<CountryRunwayResponse> list = new ArrayList<>();
        List<Object[]> countryRunway = countryRepository.countryRunway();

        countryRunway.forEach(item->{

            CountryRunwayResponse countryRunwayResponse = new CountryRunwayResponse();
            List<String> listRunAway = Arrays.asList(item[2].toString().split(","));

            countryRunwayResponse.code(
                    item[0].toString())
                    .name(item[1].toString())
                    .runwayAway(listRunAway);

            list.add(countryRunwayResponse);

        });

        return list;
    }


    @Override
    public Long count() {
        return countryRepository.count();
    }
}
