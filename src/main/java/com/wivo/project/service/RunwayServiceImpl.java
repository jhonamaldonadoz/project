package com.wivo.project.service;

import com.wivo.project.repository.RunwayRepository;
import com.wivo.project.resource.response.RunwayMostCommonResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class RunwayServiceImpl implements RunwayService {

    @Autowired
    RunwayRepository runwayRepository;


    @Override
    public List<RunwayMostCommonResponse> runwayMostCommon() {

        List<RunwayMostCommonResponse> runwayMostCommonResponsesList = new ArrayList<>();
        List<Object[]> list = runwayRepository.runwaysMostCommon();

        list.forEach(item->{

            RunwayMostCommonResponse runwayMostCommonResponses = new RunwayMostCommonResponse();
            runwayMostCommonResponses.idCountry(item[0].toString())
                    .nameCountry(item[1].toString())
                    .idAirport(item[2].toString())
                    .nameAirport(item[3].toString())
                    .idRunways(item[4].toString())
                    .identAirport(item[5].toString())
                    .leIdent(item[6].toString())
                    .countIdent(item[7].toString());


            runwayMostCommonResponsesList.add(runwayMostCommonResponses);

        });

        return runwayMostCommonResponsesList;

    }


    @Override
    public Long count() {
        return runwayRepository.count();
    }
}
