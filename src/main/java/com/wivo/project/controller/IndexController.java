package com.wivo.project.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class IndexController {

    /**
     * Index principal redireccionamos a swagger
     * @return
     */
    @RequestMapping(value={"/","/index"}, method= RequestMethod.GET)
    public String index(){
        return "redirect:/swagger-ui.html";
    }

}
