package com.wivo.project.repository;

import com.wivo.project.model.Country;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CountryRepository extends JpaRepository<Country,String>{

    @EntityGraph(value = "Country.detail")
    @Query("SELECT c FROM Country c WHERE c.name = ?1")
    Country findByName(String name);


    @EntityGraph(value = "Country.detail")
    @Query("SELECT c FROM Country c WHERE c.code = ?1")
    Country findByCode(String code);


    @Query(value = "select c.iso_country, c.name as country_name, count(a.id) as airport_cant " +
            " from countries c " +
            " inner join airports a on a.iso_country=c.iso_country " +
            " group by c.id " +
            " order by airport_cant desc, c.name " +
            " limit 10;", nativeQuery = true)
    List<Object[]> top10More();



    @Query(value = "select c.iso_country, c.name as country_name, count(a.id) as airport_cant " +
            " from countries c " +
            " inner join airports a on a.iso_country=c.iso_country " +
            " group by c.id " +
            " order by airport_cant asc, c.name " +
            " limit 10", nativeQuery = true)
    List<Object[]> top10Less();



    @Query(value = "select c.iso_country, c.name as country_name, " +
            "GROUP_CONCAT(distinct r.surface) as runways_surfaces " +
            "from countries c " +
            "inner join airports a on a.iso_country=c.iso_country " +
            "inner join runways r on r.airport_ref=a.id " +
            "group by c.iso_country " +
            "order by c.name", nativeQuery = true)
    List<Object[]> countryRunway();


}
