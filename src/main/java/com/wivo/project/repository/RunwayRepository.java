package com.wivo.project.repository;

import com.wivo.project.model.Runway;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RunwayRepository extends JpaRepository<Runway,String> {

    @Query(value = "select " +
            "  c.id, c.name, a.id as id_airport, a.name as name_airport, r.id as id_runway, r.airport_ident, r.le_ident, count(r.le_ident) as le_ident_cant " +
            "from countries c " +
            "inner join airports a on a.iso_country=c.iso_country " +
            "inner join runways r on r.airport_ref=a.id " +
            "where r.le_ident is not null and r.le_ident <> '' " +
            "group by r.le_ident, r.le_ident " +
            "order by le_ident_cant desc " +
            "limit 10", nativeQuery = true)
    List<Object[]> runwaysMostCommon();

}
