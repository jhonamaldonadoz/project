package com.wivo.project.config;

import com.wivo.project.service.AirportService;
import com.wivo.project.service.CountryService;
import com.wivo.project.service.RunwayService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.*;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.EventListener;

/**
 *  Created by Jhon Maldonado
 *
 *  Archivo de configuracion de eventos del proyecto, para este caso evaluamos cuando la aplicacion ha sido levantada
 *  y evaluamos si las tablas han sido cargadas para ejecutar los jobs.
 */
@Configuration
public class ProjectConfig {

    public static final Logger log = LoggerFactory.getLogger(ProjectConfig.class);

    @Autowired
    JobLauncher jobLauncher;

    @Autowired
    public Job countryJob;
    @Autowired
    public Job airpotJob;
    @Autowired
    public Job runwaysJob;


    @Autowired
    CountryService countryService;
    @Autowired
    AirportService airportService;
    @Autowired
    RunwayService runwayService;



    @EventListener(ApplicationReadyEvent.class)
    public void doSomethingAfterStartup(){

        try {

            if(countryService.count()==0){

                JobParameters jobParameters = new JobParametersBuilder()
                        .addLong("time", System.currentTimeMillis())
                        .toJobParameters();

                log.info("Cargamos la tabla country");
                JobExecution execution = jobLauncher.run(countryJob, jobParameters);
                log.info("Status = "+execution.getExitStatus());
            }

            if(airportService.count()==0){

                JobParameters jobParameters = new JobParametersBuilder()
                        .addLong("time1", System.currentTimeMillis())
                        .toJobParameters();

                log.info("Cargamos la tabla airpot");
                JobExecution execution1 = jobLauncher.run(airpotJob, jobParameters);
                log.info("Status = "+execution1.getExitStatus());
            }

            if(runwayService.count()==0) {

                JobParameters jobParameters = new JobParametersBuilder()
                        .addLong("time2", System.currentTimeMillis())
                        .toJobParameters();

                log.info("Cargamos la tabla runway");
                JobExecution execution2 = jobLauncher.run(runwaysJob, jobParameters);
                log.info("Status = "+execution2.getExitStatus());
            }


        } catch (JobExecutionAlreadyRunningException e) {
            e.printStackTrace();
        } catch (JobRestartException e) {
            e.printStackTrace();
        } catch (JobInstanceAlreadyCompleteException e) {
            e.printStackTrace();
        } catch (JobParametersInvalidException e) {
            e.printStackTrace();
        }


    }




}
