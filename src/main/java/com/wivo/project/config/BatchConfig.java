package com.wivo.project.config;

import com.wivo.project.job.reader.AirportReader;
import com.wivo.project.job.reader.CountryReader;
import com.wivo.project.job.reader.RunwaysReader;
import com.wivo.project.job.writer.AirportWriter;
import com.wivo.project.job.writer.CountryWriter;
import com.wivo.project.job.writer.RunwaysWriter;
import com.wivo.project.model.Airport;
import com.wivo.project.model.Country;
import com.wivo.project.model.Runway;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.annotation.PostConstruct;

/**
 *  Created by Jhon Maldonado
 *
 *  Archivos de configuracion de los batchs del proceso, en este caso se encargan de leer y guardar en BD
 *  la informacion de los csv
 */
@Configuration
@EnableBatchProcessing
public class BatchConfig {


    @Autowired
    Environment env;
    @Autowired
    JobBuilderFactory jobBuilderFactory;
    @Autowired
    StepBuilderFactory stepBuilderFactory;
    @Autowired
    JdbcTemplate jdbcTemplate;



    @PostConstruct
    public void init() {

    }


    /**
     * ========== JOBs ==========
     */
    @Bean
    public Job airpotJob(){
        return jobBuilderFactory.get("airpotJob")
                .start(airpotStep())
                .build();
    }

    @Bean
    public Job countryJob(){
        return jobBuilderFactory.get("countryJob")
                .start(countryStep())
                .build();
    }

    @Bean
    public Job runwaysJob(){
        return jobBuilderFactory.get("runwaysStep")
                .start(runwaysStep())
                .build();
    }



    /**
     * ========== STEPs ==========
     */
    public Step airpotStep(){

        AirportReader reader = new AirportReader();
        AirportWriter writer = new AirportWriter(jdbcTemplate);

        return stepBuilderFactory.get("airpotStep")
                .<Airport,Airport>chunk(5000)
                .reader(reader)
                .writer(writer)
                .build();

    }


    public Step countryStep(){

        CountryReader reader = new CountryReader();
        CountryWriter writer = new CountryWriter(jdbcTemplate);

        return stepBuilderFactory.get("countryStep")
                .<Country,Country>chunk(500)
                .reader(reader)
                .writer(writer)
                .build();

    }

    public Step runwaysStep(){

        RunwaysReader reader = new RunwaysReader();
        RunwaysWriter writer = new RunwaysWriter(jdbcTemplate);

        return stepBuilderFactory.get("runwaysStep")
                .<Runway,Runway>chunk(500)
                .reader(reader)
                .writer(writer)
                .build();

    }

}
