package com.wivo.project.job.writer;

import com.wivo.project.model.Country;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.ArrayList;
import java.util.List;

public class CountryWriter implements ItemWriter<Country> {

    public static final Logger log = LoggerFactory.getLogger(CountryWriter.class);
    public static JdbcTemplate jdbcTemplate;
    public static String query="INSERT INTO countries(id, iso_country, name, continent, wikipedia_link, keywords) " +
            " VALUES (?,?,?,?,?,?)";


    public CountryWriter(JdbcTemplate jdbcTemplate){
        this.jdbcTemplate = jdbcTemplate;
    }


    @Override
    public void write(List<? extends Country> list) throws Exception {

        List<Object[]> parameterList = new ArrayList<>();

        list.forEach(item->{

                if(item.getId()!=null)
                    parameterList.add(new Object[]{
                            item.getId(),
                            item.getCode(),
                            item.getName(),
                            item.getContinent(),
                            item.getWikipediaLink(),
                            item.getKeywords()
                    });

        });

        jdbcTemplate.batchUpdate(query,parameterList);

    }

}
