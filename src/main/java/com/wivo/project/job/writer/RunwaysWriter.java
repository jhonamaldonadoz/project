package com.wivo.project.job.writer;

import com.wivo.project.model.Runway;
import org.springframework.batch.item.ItemWriter;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.ArrayList;
import java.util.List;

public class RunwaysWriter implements ItemWriter<Runway> {

    public static JdbcTemplate jdbcTemplate;
    public String query="INSERT INTO runways(id, airport_ref, airport_ident, length_ft, width_ft, " +
            " surface, lighted, closed, le_ident, le_latitude_deg, " +
            " le_longitude_deg, le_elevation_ft, le_heading_degT, le_displaced_threshold_ft, he_ident, " +
            "he_latitude_deg, he_longitude_deg, he_elevation_ft, he_heading_degT, he_displaced_threshold_ft) " +
            " VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

    public RunwaysWriter(JdbcTemplate jdbcTemplate){
        this.jdbcTemplate = jdbcTemplate;
    }


    @Override
    public void write(List<? extends Runway> list) throws Exception {

        List<Object[]> parameterList = new ArrayList<>();

        list.forEach(item->{

            parameterList.add(new Object[]{
                    item.getId(),
                    item.getAirportRef(),
                    item.getAirportIdent(),
                    item.getLengthFt(),
                    item.getWidthFt(),
                    item.getSurface(),
                    item.getLighted(),
                    item.getClosed(),
                    item.getLeIdent(),
                    item.getLeLatitudeDeg(),
                    item.getLeLongitudeDeg(),
                    item.getLeElevationFt(),
                    item.getHeHeadingDegT(),
                    item.getLeDisplacedThresholdFt(),
                    item.getHeIdent(),
                    item.getHeLatitudeDeg(),
                    item.getHeLongitudeDeg(),
                    item.getHeElevationFt(),
                    item.getHeHeadingDegT(),
                    item.getHeDisplacedThresholdFt()
            });
        });

        jdbcTemplate.batchUpdate(query,parameterList);

    }
}
