package com.wivo.project.job.writer;

import com.wivo.project.model.Airport;
import org.springframework.batch.item.ItemWriter;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.ArrayList;
import java.util.List;

public class AirportWriter implements ItemWriter<Airport> {

    public static JdbcTemplate jdbcTemplate;
    public String query ="INSERT INTO airports(id,ident,airport_type,name,latitud_deg,longitud_deg,elevation_ft,continent,iso_country,iso_region,municipality," +
            " scheduled_service,gps_code,iata_code,local_code,home_link,wikipedia_link,keywords) " +
            " VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";


    public AirportWriter(JdbcTemplate jdbcTemplate){
        this.jdbcTemplate = jdbcTemplate;
    }


    @Override
    public void write(List<? extends Airport> list) throws Exception {
        List<Object[]> parameterList = new ArrayList<>();

        list.forEach(item->{
            parameterList.add(new Object[]{
                    item.getId(),
                    item.getIdent(),
                    item.getAirportType(),
                    item.getName(),
                    item.getLatitudDeg(),
                    item.getLongitudDeg(),
                    item.getElevationFt(),
                    item.getContinent(),
                    item.getIsoCountry(),
                    item.getIsoRegion(),
                    item.getMunicipality(),
                    item.getScheduledService(),
                    item.getGpsCode(),
                    item.getIataCode(),
                    item.getLocalCode(),
                    item.getHomeLink(),
                    item.getWikipediaLink(),
                    item.getKeywords()
            });
        });

        jdbcTemplate.batchUpdate(query,parameterList);
    }

}
