package com.wivo.project.job.mapper;

import com.wivo.project.model.Runway;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;
import org.springframework.validation.BindException;

public class RunwaysMapper implements FieldSetMapper<Runway> {

    @Override
    public Runway mapFieldSet(FieldSet fieldSet) throws BindException {
        Runway runway = new Runway();

        runway.id(fieldSet.readString("id"))
                .airportRef(fieldSet.readString("airport_ref"))
                .airportIdent(fieldSet.readString("airport_ident"))
                .lengthFt(fieldSet.readString("length_ft"))
                .widthFt(fieldSet.readString("width_ft"))

                .surface(fieldSet.readString("surface"))
                .lighted(fieldSet.readString("lighted"))
                .closed(fieldSet.readString("closed"))
                .leIdent(fieldSet.readString("le_ident"))
                .leLatitudeDeg(fieldSet.readString("le_latitude_deg"))

                .leLongitudeDeg(fieldSet.readString("le_longitude_deg"))
                .leElevationFt(fieldSet.readString("le_elevation_ft"))
                .leHeadingDegT(fieldSet.readString("le_heading_degT"))
                .leDisplacedThresholdFt(fieldSet.readString("le_displaced_threshold_ft"))
                .heIdent(fieldSet.readString("he_ident"))

                .heLatitudeDeg(fieldSet.readString("he_latitude_deg"))
                .heLongitudeDeg(fieldSet.readString("he_longitude_deg"))
                .heElevationFt(fieldSet.readString("he_elevation_ft"))
                .heHeadingDegT(fieldSet.readString("he_heading_degT"))
                .heDisplacedThresholdFt(fieldSet.readString("he_displaced_threshold_ft"));

        return runway;
    }
}
