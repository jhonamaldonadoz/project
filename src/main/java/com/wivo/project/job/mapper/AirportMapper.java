package com.wivo.project.job.mapper;

import com.wivo.project.model.Airport;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;
import org.springframework.validation.BindException;

public class AirportMapper implements FieldSetMapper<Airport> {
    @Override
    public Airport mapFieldSet(FieldSet fieldSet) throws BindException {

        Airport airport = new Airport();

        airport.setId(fieldSet.readString("id"));
        airport.setIdent(fieldSet.readString("ident"));
        airport.setAirportType(fieldSet.readString("type"));
        airport.setName(fieldSet.readString("name"));
        airport.setLatitudDeg(fieldSet.readString("latitude_deg"));
        airport.setLongitudDeg(fieldSet.readString("longitude_deg"));
        airport.setElevationFt(fieldSet.readString("elevation_ft"));
        airport.setContinent(fieldSet.readString("continent"));
        airport.setIsoCountry(fieldSet.readString("iso_country"));
        airport.setIsoTegion(fieldSet.readString("iso_region"));
        airport.setMunicipality(fieldSet.readString("municipality"));
        airport.setScheduledService(fieldSet.readString("scheduled_service"));
        airport.setGpsCode(fieldSet.readString("gps_code"));
        airport.setIataCode(fieldSet.readString("iata_code"));
        airport.setLocalCode(fieldSet.readString("local_code"));
        airport.setHomeLink(fieldSet.readString("home_link"));
        airport.setWikipediaLink(fieldSet.readString("wikipedia_link"));
        airport.setKeywords(fieldSet.readString("keywords"));

        return airport;
    }
}
