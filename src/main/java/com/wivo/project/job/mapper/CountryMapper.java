package com.wivo.project.job.mapper;

import com.wivo.project.model.Country;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;
import org.springframework.validation.BindException;

public class CountryMapper implements FieldSetMapper<Country> {

    @Override
    public Country mapFieldSet(FieldSet fieldSet) throws BindException {

        Country country = new Country();

        country.id(fieldSet.readString("id"))
                .code(fieldSet.readString("code"))
                .name(fieldSet.readString("name"))
                .continent(fieldSet.readString("continent"))
                .wikipediaLink(fieldSet.readString("wikipedia_link"))
                .keywords(fieldSet.readString("keywords"));

        return country;

    }
}
