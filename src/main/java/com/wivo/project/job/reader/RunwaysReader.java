package com.wivo.project.job.reader;

import com.wivo.project.job.mapper.RunwaysMapper;
import com.wivo.project.model.Runway;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.*;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.core.io.ClassPathResource;

public class RunwaysReader implements ItemStreamReader<Runway> {

    private static Logger log = LoggerFactory.getLogger(RunwaysReader.class);
    private static String[] names = new String[]{
            "id","airport_ref","airport_ident","length_ft","width_ft","surface","lighted","closed","le_ident",
            "le_latitude_deg","le_longitude_deg","le_elevation_ft","le_heading_degT","le_displaced_threshold_ft",
            "he_ident","he_latitude_deg","he_longitude_deg","he_elevation_ft","he_heading_degT","he_displaced_threshold_ft",
    };

    FlatFileItemReader<Runway> itemReader;
    DefaultLineMapper<Runway> lineMapper;
    DelimitedLineTokenizer lineTokenizer;

    @Override
    public void open(ExecutionContext executionContext) throws ItemStreamException {

        log.info("Inicio proceso leer cvs Runways");

        itemReader = new FlatFileItemReader<>();
        lineMapper = new DefaultLineMapper<>();
        lineTokenizer = new DelimitedLineTokenizer();

        lineTokenizer.setDelimiter(DelimitedLineTokenizer.DELIMITER_COMMA);
        lineTokenizer.setQuoteCharacter(DelimitedLineTokenizer.DEFAULT_QUOTE_CHARACTER);
        lineTokenizer.setNames(names);

        lineMapper.setLineTokenizer(lineTokenizer);
        lineMapper.setFieldSetMapper(new RunwaysMapper());

        itemReader.setResource(new ClassPathResource("db/csv/runways.csv"));
        itemReader.setLineMapper(lineMapper);
        itemReader.setLinesToSkip(1);

        itemReader.open(new ExecutionContext());
    }

    @Override
    public Runway read() throws Exception, UnexpectedInputException, ParseException, NonTransientResourceException {

        Runway data =  itemReader.read();
        return  data;
    }

    @Override
    public void update(ExecutionContext executionContext) throws ItemStreamException {

    }

    @Override
    public void close() throws ItemStreamException {
        log.info("Fin proceso leer cvs Runways");
        itemReader.close();
    }
}
