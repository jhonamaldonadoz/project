package com.wivo.project.job.reader;

import com.wivo.project.job.mapper.CountryMapper;
import com.wivo.project.model.Country;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.*;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.core.io.ClassPathResource;

public class CountryReader implements ItemStreamReader<Country> {

    private static Logger log = LoggerFactory.getLogger(CountryReader.class);
    private String[] names = new String[] {"id","code","name","continent","wikipedia_link","keywords"};

    FlatFileItemReader<Country> itemReader;
    DefaultLineMapper<Country> lineMapper;
    DelimitedLineTokenizer lineTokenizer;


    @Override
    public void open(ExecutionContext executionContext) throws ItemStreamException {

        log.info("Inicio proceso leer cvs Country");

        itemReader = new FlatFileItemReader<>();
        lineMapper = new DefaultLineMapper<>();
        lineTokenizer = new DelimitedLineTokenizer();

        lineTokenizer.setDelimiter(DelimitedLineTokenizer.DELIMITER_COMMA);
        lineTokenizer.setQuoteCharacter(DelimitedLineTokenizer.DEFAULT_QUOTE_CHARACTER);
        lineTokenizer.setNames(names);

        lineMapper.setLineTokenizer(lineTokenizer);
        lineMapper.setFieldSetMapper(new CountryMapper());

        itemReader.setResource(new ClassPathResource("db/csv/countries.csv"));
        itemReader.setLineMapper(lineMapper);
        itemReader.setLinesToSkip(1);

        itemReader.open(new ExecutionContext());
    }

    @Override
    public Country read() throws Exception, UnexpectedInputException, ParseException, NonTransientResourceException {
        Country data = itemReader.read();
        return  data;
    }

    @Override
    public void update(ExecutionContext executionContext) throws ItemStreamException {

    }

    @Override
    public void close() throws ItemStreamException {
        log.info("Fin proceso leer cvs Country");
        itemReader.close();
    }
}
