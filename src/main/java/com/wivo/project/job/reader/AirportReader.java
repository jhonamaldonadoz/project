package com.wivo.project.job.reader;


import com.wivo.project.job.mapper.AirportMapper;
import com.wivo.project.model.Airport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.*;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.core.io.ClassPathResource;

public class AirportReader implements ItemStreamReader<Airport> {

    private static Logger log = LoggerFactory.getLogger(AirportReader.class);
    private String[] names = new String[] {
            "id","ident","type","name","latitude_deg","longitude_deg","elevation_ft",
            "continent","iso_country","iso_region","municipality","scheduled_service",
            "gps_code","iata_code","local_code","home_link","wikipedia_link","keywords"
    };

    FlatFileItemReader<Airport> itemReader;
    DefaultLineMapper<Airport> lineMapper;
    DelimitedLineTokenizer lineTokenizer;


    @Override
    public void open(ExecutionContext executionContext) throws ItemStreamException {

        log.info("Inicio proceso leer cvs Airport");

        itemReader = new FlatFileItemReader<>();
        lineMapper = new DefaultLineMapper<>();
        lineTokenizer = new DelimitedLineTokenizer();

        lineTokenizer.setDelimiter(DelimitedLineTokenizer.DELIMITER_COMMA);
        lineTokenizer.setQuoteCharacter(DelimitedLineTokenizer.DEFAULT_QUOTE_CHARACTER);
        lineTokenizer.setNames(names);

        lineMapper.setLineTokenizer(lineTokenizer);
        lineMapper.setFieldSetMapper(new AirportMapper());

        itemReader.setResource(new ClassPathResource("db/csv/airports.csv"));
        itemReader.setLineMapper(lineMapper);
        itemReader.setLinesToSkip(1);

        itemReader.open(new ExecutionContext());

    }

    @Override
    public Airport read() throws Exception, UnexpectedInputException, ParseException, NonTransientResourceException {
        Airport data =  itemReader.read();
        return  data;
    }


    @Override
    public void update(ExecutionContext executionContext) throws ItemStreamException {

    }

    @Override
    public void close() throws ItemStreamException {
        log.info("Fin proceso leer cvs Airport");
        itemReader.close();
    }

}
