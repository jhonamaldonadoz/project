package com.wivo.project;

import com.wivo.project.config.BatchConfig;
import com.wivo.project.config.ProjectConfig;
import com.wivo.project.config.SawggerConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

@SpringBootApplication
@Import(value ={SawggerConfig.class, BatchConfig.class, ProjectConfig.class})
public class ProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProjectApplication.class, args);
	}

}
