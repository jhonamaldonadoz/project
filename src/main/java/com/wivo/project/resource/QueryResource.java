package com.wivo.project.resource;

import com.wivo.project.model.Country;
import com.wivo.project.service.CountryService;
import io.swagger.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *  Created by Jhon Maldonado
 *  controlares rest
 */
@RestController
@RequestMapping(value = "/rest/query")
@Api(value ="/rest/query", tags = "query")
public class QueryResource {

    private final static Logger log = LoggerFactory.getLogger(QueryResource.class);


    @Autowired
    CountryService countryService;


    @ApiOperation(
            value = "Aeropuerto y pistas por pais",
            notes = "Retornará los aeropuertos del mismo y las pistas en cada aeropuerto")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = Country.class),
    })
    @ApiImplicitParams({
            @ApiImplicitParam(name = "tipoBusqueda", value = "Definimos el tipo de busqueda a realizar", allowableValues = "name,code", required = true, paramType = "path"),
            @ApiImplicitParam(name = "busqueda", value = "Si introduce el codigo del pais debe ser con el siguiente stander: ISO 3166-1 alfa-2, ejemplo Venezuela -> VE",
                    required = true, paramType = "path")
    })
    @RequestMapping(value = "/aeropuertos-pistas-por-pais/{tipoBusqueda:(?:name|code)}/{busqueda}",
            headers = "Accept=application/json", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> aeropuetosPistasPorPais(@PathVariable String tipoBusqueda,@PathVariable String busqueda){

        log.info("Parametro tipo de busqueda '{}' parametro busqueda '{}' = ",tipoBusqueda,busqueda);

        ResponseEntity<?> response;
        Country country = countryService.findByType(tipoBusqueda,busqueda);

        //verifcamos la existencia
        if(country==null){
            response = new ResponseEntity<>(HttpStatus.ACCEPTED);
        } else {
            log.info("Country = {}", country.getName());
            log.info("Count airport = {}", country.getAirports().size());

            response = new ResponseEntity<>(country,HttpStatus.OK);
        }

        return response;

    } // end method aeropuetosPistasPorPais

} // end class QueryResource
