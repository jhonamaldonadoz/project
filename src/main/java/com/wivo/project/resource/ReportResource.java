package com.wivo.project.resource;


import com.wivo.project.model.Country;
import com.wivo.project.resource.response.CountryRunwayResponse;
import com.wivo.project.resource.response.CountryTopResponse;
import com.wivo.project.resource.response.RunwayMostCommonResponse;
import com.wivo.project.service.CountryService;
import com.wivo.project.service.RunwayService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/rest/report")
@Api(value ="/rest/report", tags = "report")
public class ReportResource {

    private final static Logger log = LoggerFactory.getLogger(ReportResource.class);


    @Autowired
    CountryService countryService;

    @Autowired
    RunwayService runwayService;



    @ApiOperation(
            value = "Top 10 de países con el mayor número de aeropuertos ",
            notes = "Top 10 de países con el mayor número de aeropuertos (acompañado de la cifra)")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = CountryTopResponse.class),
        })
    @RequestMapping(value = "/paises-mayor-numero-aeropuertos",
            headers = "Accept=application/json", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> paisesMayorNumeroAeropuertos(){

        log.info("Ingresamos a paises-mayor-numero-aeropuertos");

        ResponseEntity<?> response;

        List<CountryTopResponse> countryList = countryService.top10Most();

        if(countryList.size()>0){
            response = new ResponseEntity<Object>(countryList, HttpStatus.OK);
        } else {
            response = new ResponseEntity<>(HttpStatus.ACCEPTED);
        }

        return response;

    } // end method paisesMayorNumeroAeropuertos




    @ApiOperation(
            value = "Top 10 de países con el menor número de aeropuertos ",
            notes = "Top 10 de países con el menor número de aeropuertos (acompañado de la cifra)")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = Country.class),
    })
    @RequestMapping(value = "/paises-menor-numero-aeropuertos",
            headers = "Accept=application/json", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> paisesMenorNumeroAeropuertos(){

        log.info("Ingresamos a paises-menor-numero-aeropuertos");

        ResponseEntity<?> response;

        List<CountryTopResponse> countryList = countryService.top10Less();

        if(countryList.size()>0){
            response = new ResponseEntity<Object>(countryList, HttpStatus.OK);
        } else {
            response = new ResponseEntity<>(HttpStatus.ACCEPTED);
        }

        return response;

    } // end method paisesMenorNumeroAeropuertos




    @ApiOperation(
            value = "Tipos de pistas por país",
            notes = "Tipos de pistas por país")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = Country.class),
        })
    @RequestMapping(value = "/tipo-pista-por-pais",
            headers = "Accept=application/json", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> tipoPistarPorPais(){

        log.info("Ingresamos a tipo-pista-por-pais");
        ResponseEntity<?> response;

        List<CountryRunwayResponse> list = countryService.countryRunway();

        if(list.size()>0){
            response = new ResponseEntity<Object>(list, HttpStatus.OK);
        } else {
            response = new ResponseEntity<>(HttpStatus.ACCEPTED);
        }

        return response;

    } // end method tipoPistarPorPais




    @ApiOperation(
            value = "Top 10 de tipos de pistas más comunes",
            notes = "Top 10 de tipos de pistas más comunes")
    @ApiResponses(
            value = {
            @ApiResponse(code = 200, message = "Ok", response = Country.class),
        })
    @RequestMapping(value = "/tipo-pistas-mas-comunes",
            headers = "Accept=application/json", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> tiposDePistasMasComunes(){

        log.info("Ingresamos a tipo-pistas-mas-comunes");
        ResponseEntity<?> response;

        List<RunwayMostCommonResponse> list =  runwayService.runwayMostCommon();

        if(list.size()>0){
            response = new ResponseEntity<Object>(list, HttpStatus.OK);
        } else {
            response = new ResponseEntity<>(HttpStatus.ACCEPTED);
        }

        return response;

    } // end method tiposDePistasMasComunes


}
