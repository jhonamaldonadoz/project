package com.wivo.project.resource.response;

public class RunwayMostCommonResponse {

    public String idCountry;
    public String nameCountry;
    public String idAirport;
    public String nameAirport;
    public String idRunways;
    public String identAirport;
    public String leIdent;
    public String countIdent;



    public String getIdCountry() {
        return idCountry;
    }

    public void setIdCountry(String idCountry) {
        this.idCountry = idCountry;
    }

    public String getNameCountry() {
        return nameCountry;
    }

    public void setNameCountry(String nameCountry) {
        this.nameCountry = nameCountry;
    }

    public String getIdAirport() {
        return idAirport;
    }

    public void setIdAirport(String idAirport) {
        this.idAirport = idAirport;
    }

    public String getNameAirport() {
        return nameAirport;
    }

    public void setNameAirport(String nameAirport) {
        this.nameAirport = nameAirport;
    }

    public String getIdRunways() {
        return idRunways;
    }

    public void setIdRunways(String idRunways) {
        this.idRunways = idRunways;
    }

    public String getIdentAirport() {
        return identAirport;
    }

    public void setIdentAirport(String identAirport) {
        this.identAirport = identAirport;
    }

    public String getLeIdent() {
        return leIdent;
    }

    public void setLeIdent(String leIdent) {
        this.leIdent = leIdent;
    }

    public String getCountIdent() {
        return countIdent;
    }

    public void setCountIdent(String countIdent) {
        this.countIdent = countIdent;
    }




    public RunwayMostCommonResponse idCountry(final String idCountry) {
        this.idCountry = idCountry;
        return this;
    }

    public RunwayMostCommonResponse nameCountry(final String nameCountry) {
        this.nameCountry = nameCountry;
        return this;
    }

    public RunwayMostCommonResponse idAirport(final String idAirport) {
        this.idAirport = idAirport;
        return this;
    }

    public RunwayMostCommonResponse nameAirport(final String nameAirport) {
        this.nameAirport = nameAirport;
        return this;
    }

    public RunwayMostCommonResponse idRunways(final String idRunways) {
        this.idRunways = idRunways;
        return this;
    }

    public RunwayMostCommonResponse identAirport(final String identAirport) {
        this.identAirport = identAirport;
        return this;
    }

    public RunwayMostCommonResponse leIdent(final String leIdent) {
        this.leIdent = leIdent;
        return this;
    }

    public RunwayMostCommonResponse countIdent(final String countIdent) {
        this.countIdent = countIdent;
        return this;
    }


}
