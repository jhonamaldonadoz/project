package com.wivo.project.resource.response;

import java.util.List;

public class CountryRunwayResponse {

    private String code;
    private String name;
    private List<String> runwayAway;

    public String getCode() {
        return code;
    }
    public void setCode(String code) {
        this.code = code;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public List<String> getRunwayAway() {
        return runwayAway;
    }



    public void setRunwayAway(List<String> runwayAway) {
        this.runwayAway = runwayAway;
    }

    public CountryRunwayResponse code(final String code) {
        this.code = code;
        return this;
    }

    public CountryRunwayResponse name(final String name) {
        this.name = name;
        return this;
    }

    public CountryRunwayResponse runwayAway(final List<String> runwayAway) {
        this.runwayAway = runwayAway;
        return this;
    }


    @Override
    public String toString() {
        return "CountryRunwayResponse{" +
                "code='" + code + '\'' +
                ", name='" + name + '\'' +
                ", runwayAway=" + runwayAway +
                '}';
    }
}
