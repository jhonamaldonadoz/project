package com.wivo.project.resource.response;

public class CountryTopResponse {

    private String nameCountry;
    private int count;


    public String getNameCountry() {
        return nameCountry;
    }

    public void setNameCountry(String nameCountry) {
        this.nameCountry = nameCountry;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }



    public CountryTopResponse nameCountry(final String nameCountry) {
        this.nameCountry = nameCountry;
        return this;
    }

    public CountryTopResponse count(final int count) {
        this.count = count;
        return this;
    }


    @Override
    public String toString() {
        return "CountryTopResponse{" +
                "nameCountry='" + nameCountry + '\'' +
                ", count=" + count +
                '}';
    }

}
