package com.wivo.project.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "airports")
public class Airport implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id")
    private String id;
    @Column(name = "ident")
    private String ident;
    @Column(name = "airport_type")
    private String airportType;
    @Column(name = "name")
    private String name;
    @Column(name = "latitud_deg")
    private String latitudDeg;
    @Column(name = "longitud_deg")
    private String longitudDeg;
    @Column(name = "elevation_ft")
    private String elevationFt;
    @Column(name = "continent")
    private String continent;
    @Column(name = "iso_country")
    private String isoCountry;
    @Column(name = "iso_region")
    private String isoRegion;
    @Column(name = "municipality")
    private String municipality;
    @Column(name = "scheduled_service")
    private String scheduledService;
    @Column(name = "gps_code")
    private String gpsCode;
    @Column(name = "iata_code")
    private String iataCode;
    @Column(name = "local_code")
    private String localCode;
    @Column(name = "home_link")
    private String homeLink;
    @Column(name = "wikipedia_link")
    private String wikipediaLink;
    @Column(name = "keywords")
    private String keywords;

    @ManyToOne
    @JoinColumn(name = "iso_country", referencedColumnName ="iso_country", updatable=false, insertable=false)
    @JsonBackReference
    private Country country;

    @OneToMany(mappedBy = "airport", fetch = FetchType.LAZY)
    @JsonManagedReference
    List<Runway> runways;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdent() {
        return ident;
    }

    public void setIdent(String ident) {
        this.ident = ident;
    }

    public String getAirportType() {
        return airportType;
    }

    public void setAirportType(String airportType) {
        this.airportType = airportType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLatitudDeg() {
        return latitudDeg;
    }

    public void setLatitudDeg(String latitudDeg) {
        this.latitudDeg = latitudDeg;
    }

    public String getLongitudDeg() {
        return longitudDeg;
    }

    public void setLongitudDeg(String longitudDeg) {
        this.longitudDeg = longitudDeg;
    }

    public String getElevationFt() {
        return elevationFt;
    }

    public void setElevationFt(String elevationFt) {
        this.elevationFt = elevationFt;
    }

    public String getContinent() {
        return continent;
    }

    public void setContinent(String continent) {
        this.continent = continent;
    }

    public String getIsoCountry() {
        return isoCountry;
    }

    public void setIsoCountry(String isoCountry) {
        this.isoCountry = isoCountry;
    }

    public String getIsoRegion() {
        return isoRegion;
    }

    public void setIsoTegion(String isoRegion) {
        this.isoRegion = isoRegion;
    }

    public String getMunicipality() {
        return municipality;
    }

    public void setMunicipality(String municipality) {
        this.municipality = municipality;
    }

    public String getScheduledService() {
        return scheduledService;
    }

    public void setScheduledService(String scheduledService) {
        this.scheduledService = scheduledService;
    }

    public String getGpsCode() {
        return gpsCode;
    }

    public void setGpsCode(String gpsCode) {
        this.gpsCode = gpsCode;
    }

    public String getIataCode() {
        return iataCode;
    }

    public void setIataCode(String iataCode) {
        this.iataCode = iataCode;
    }

    public String getLocalCode() {
        return localCode;
    }

    public void setLocalCode(String localCode) {
        this.localCode = localCode;
    }

    public String getHomeLink() {
        return homeLink;
    }

    public void setHomeLink(String homeLink) {
        this.homeLink = homeLink;
    }

    public String getWikipediaLink() {
        return wikipediaLink;
    }

    public void setWikipediaLink(String wikipediaLink) {
        this.wikipediaLink = wikipediaLink;
    }

    public String getKeywords() {
        return keywords;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    public void setIsoRegion(String isoRegion) {
        this.isoRegion = isoRegion;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public List<Runway> getRunways() {
        return runways;
    }

    public void setRunways(List<Runway> runways) {
        this.runways = runways;
    }









    public Airport id(final String id) {
        this.id = id;
        return this;
    }

    public Airport ident(final String ident) {
        this.ident = ident;
        return this;
    }

    public Airport airportType(final String airportType) {
        this.airportType = airportType;
        return this;
    }

    public Airport name(final String name) {
        this.name = name;
        return this;
    }

    public Airport latitudDeg(final String latitudDeg) {
        this.latitudDeg = latitudDeg;
        return this;
    }

    public Airport longitudDeg(final String longitudDeg) {
        this.longitudDeg = longitudDeg;
        return this;
    }

    public Airport elevationFt(final String elevationFt) {
        this.elevationFt = elevationFt;
        return this;
    }

    public Airport continent(final String continent) {
        this.continent = continent;
        return this;
    }

    public Airport isoCountry(final String isoCountry) {
        this.isoCountry = isoCountry;
        return this;
    }

    public Airport isoRegion(final String isoRegion) {
        this.isoRegion = isoRegion;
        return this;
    }

    public Airport municipality(final String municipality) {
        this.municipality = municipality;
        return this;
    }

    public Airport scheduledService(final String scheduledService) {
        this.scheduledService = scheduledService;
        return this;
    }

    public Airport gpsCode(final String gpsCode) {
        this.gpsCode = gpsCode;
        return this;
    }

    public Airport iataCode(final String iataCode) {
        this.iataCode = iataCode;
        return this;
    }

    public Airport localCode(final String localCode) {
        this.localCode = localCode;
        return this;
    }

    public Airport homeLink(final String homeLink) {
        this.homeLink = homeLink;
        return this;
    }

    public Airport wikipediaLink(final String wikipediaLink) {
        this.wikipediaLink = wikipediaLink;
        return this;
    }

    public Airport keywords(final String keywords) {
        this.keywords = keywords;
        return this;
    }


}
