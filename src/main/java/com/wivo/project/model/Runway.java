package com.wivo.project.model;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "runways")
public class Runway implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id")
    private String id;
    @Column(name = "airport_ref")
    private String airportRef;
    @Column(name = "airport_ident")
    private String airportIdent;
    @Column(name = "length_ft")
    private String lengthFt;
    @Column(name = "width_ft")
    private String widthFt;
    @Column(name = "surface")
    private String surface;
    @Column(name = "lighted")
    private String lighted;
    @Column(name = "closed")
    private String closed;
    @Column(name = "le_ident")
    private String leIdent;
    @Column(name = "le_latitude_deg")
    private String leLatitudeDeg;
    @Column(name = "le_longitude_deg")
    private String leLongitudeDeg;
    @Column(name = "le_elevation_ft")
    private String leElevationFt;
    @Column(name = "le_heading_degT")
    private String leHeadingDegT;
    @Column(name = "le_displaced_threshold_ft")
    private String leDisplacedThresholdFt;
    @Column(name = "he_ident")
    private String heIdent;
    @Column(name = "he_latitude_deg")
    private String heLatitudeDeg;
    @Column(name = "he_longitude_deg")
    private String heLongitudeDeg;
    @Column(name = "he_elevation_ft")
    private String heElevationFt;
    @Column(name = "he_heading_degT")
    private String heHeadingDegT;
    @Column(name = "he_displaced_threshold_ft")
    private String heDisplacedThresholdFt;

    @ManyToOne
    @JoinColumn(name = "airport_ref", referencedColumnName ="id", updatable=false, insertable=false)
    @JsonBackReference
    private Airport airport;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAirportRef() {
        return airportRef;
    }

    public void setAirportRef(String airportRef) {
        this.airportRef = airportRef;
    }

    public String getAirportIdent() {
        return airportIdent;
    }

    public void setAirportIdent(String airportIdent) {
        this.airportIdent = airportIdent;
    }

    public String getLengthFt() {
        return lengthFt;
    }

    public void setLengthFt(String lengthFt) {
        this.lengthFt = lengthFt;
    }

    public String getWidthFt() {
        return widthFt;
    }

    public void setWidthFt(String widthFt) {
        this.widthFt = widthFt;
    }

    public String getSurface() {
        return surface;
    }

    public void setSurface(String surface) {
        this.surface = surface;
    }

    public String getLighted() {
        return lighted;
    }

    public void setLighted(String lighted) {
        this.lighted = lighted;
    }

    public String getClosed() {
        return closed;
    }

    public void setClosed(String closed) {
        this.closed = closed;
    }

    public String getLeIdent() {
        return leIdent;
    }

    public void setLeIdent(String leIdent) {
        this.leIdent = leIdent;
    }

    public String getLeLatitudeDeg() {
        return leLatitudeDeg;
    }

    public void setLeLatitudeDeg(String leLatitudeDeg) {
        this.leLatitudeDeg = leLatitudeDeg;
    }

    public String getLeLongitudeDeg() {
        return leLongitudeDeg;
    }

    public void setLeLongitudeDeg(String leLongitudeDeg) {
        this.leLongitudeDeg = leLongitudeDeg;
    }

    public String getLeElevationFt() {
        return leElevationFt;
    }

    public void setLeElevationFt(String leElevationFt) {
        this.leElevationFt = leElevationFt;
    }

    public String getLeHeadingDegT() {
        return leHeadingDegT;
    }

    public void setLeHeadingDegT(String leHeadingDegT) {
        this.leHeadingDegT = leHeadingDegT;
    }

    public String getLeDisplacedThresholdFt() {
        return leDisplacedThresholdFt;
    }

    public void setLeDisplacedThresholdFt(String leDisplacedThresholdFt) {
        this.leDisplacedThresholdFt = leDisplacedThresholdFt;
    }

    public String getHeIdent() {
        return heIdent;
    }

    public void setHeIdent(String heIdent) {
        this.heIdent = heIdent;
    }

    public String getHeLatitudeDeg() {
        return heLatitudeDeg;
    }

    public void setHeLatitudeDeg(String heLatitudeDeg) {
        this.heLatitudeDeg = heLatitudeDeg;
    }

    public String getHeLongitudeDeg() {
        return heLongitudeDeg;
    }

    public void setHeLongitudeDeg(String heLongitudeDeg) {
        this.heLongitudeDeg = heLongitudeDeg;
    }

    public String getHeElevationFt() {
        return heElevationFt;
    }

    public void setHeElevationFt(String heElevationFt) {
        this.heElevationFt = heElevationFt;
    }

    public String getHeHeadingDegT() {
        return heHeadingDegT;
    }

    public void setHeHeadingDegT(String heHeadingDegT) {
        this.heHeadingDegT = heHeadingDegT;
    }

    public String getHeDisplacedThresholdFt() {
        return heDisplacedThresholdFt;
    }

    public void setHeDisplacedThresholdFt(String heDisplacedThresholdFt) {
        this.heDisplacedThresholdFt = heDisplacedThresholdFt;
    }




    public Runway id(final String id) {
        this.id = id;
        return this;
    }

    public Runway airportRef(final String airportRef) {
        this.airportRef = airportRef;
        return this;
    }

    public Runway airportIdent(final String airportIdent) {
        this.airportIdent = airportIdent;
        return this;
    }

    public Runway lengthFt(final String lengthFt) {
        this.lengthFt = lengthFt;
        return this;
    }

    public Runway widthFt(final String widthFt) {
        this.widthFt = widthFt;
        return this;
    }

    public Runway surface(final String surface) {
        this.surface = surface;
        return this;
    }

    public Runway lighted(final String lighted) {
        this.lighted = lighted;
        return this;
    }

    public Runway closed(final String closed) {
        this.closed = closed;
        return this;
    }

    public Runway leIdent(final String leIdent) {
        this.leIdent = leIdent;
        return this;
    }

    public Runway leLatitudeDeg(final String leLatitudeDeg) {
        this.leLatitudeDeg = leLatitudeDeg;
        return this;
    }

    public Runway leLongitudeDeg(final String leLongitudeDeg) {
        this.leLongitudeDeg = leLongitudeDeg;
        return this;
    }

    public Runway leElevationFt(final String leElevationFt) {
        this.leElevationFt = leElevationFt;
        return this;
    }

    public Runway leHeadingDegT(final String leHeadingDegT) {
        this.leHeadingDegT = leHeadingDegT;
        return this;
    }

    public Runway leDisplacedThresholdFt(final String leDisplacedThresholdFt) {
        this.leDisplacedThresholdFt = leDisplacedThresholdFt;
        return this;
    }

    public Runway heIdent(final String heIdent) {
        this.heIdent = heIdent;
        return this;
    }

    public Runway heLatitudeDeg(final String heLatitudeDeg) {
        this.heLatitudeDeg = heLatitudeDeg;
        return this;
    }

    public Runway heLongitudeDeg(final String heLongitudeDeg) {
        this.heLongitudeDeg = heLongitudeDeg;
        return this;
    }

    public Runway heElevationFt(final String heElevationFt) {
        this.heElevationFt = heElevationFt;
        return this;
    }

    public Runway heHeadingDegT(final String heHeadingDegT) {
        this.heHeadingDegT = heHeadingDegT;
        return this;
    }

    public Runway heDisplacedThresholdFt(final String heDisplacedThresholdFt) {
        this.heDisplacedThresholdFt = heDisplacedThresholdFt;
        return this;
    }


    @Override
    public String toString() {
        return "Runway{" +
                "id='" + id + '\'' +
                ", airportRef='" + airportRef + '\'' +
                ", airportIdent='" + airportIdent + '\'' +
                ", lengthFt='" + lengthFt + '\'' +
                ", widthFt='" + widthFt + '\'' +
                ", surface='" + surface + '\'' +
                ", lighted='" + lighted + '\'' +
                ", closed='" + closed + '\'' +
                ", leIdent='" + leIdent + '\'' +
                ", leLatitudeDeg='" + leLatitudeDeg + '\'' +
                ", leLongitudeDeg='" + leLongitudeDeg + '\'' +
                ", leElevationFt='" + leElevationFt + '\'' +
                ", leHeadingDegT='" + leHeadingDegT + '\'' +
                ", leDisplacedThresholdFt='" + leDisplacedThresholdFt + '\'' +
                ", heIdent='" + heIdent + '\'' +
                ", heLatitudeDeg='" + heLatitudeDeg + '\'' +
                ", heLongitudeDeg='" + heLongitudeDeg + '\'' +
                ", heElevationFt='" + heElevationFt + '\'' +
                ", heHeadingDegT='" + heHeadingDegT + '\'' +
                ", heDisplacedThresholdFt='" + heDisplacedThresholdFt + '\'' +
                '}';
    }
}
