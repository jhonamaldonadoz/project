package com.wivo.project.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "countries")
@NamedEntityGraph(name = "Country.detail", attributeNodes = @NamedAttributeNode("airports"))
public class Country implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id")
    private String id;
    @Column(name = "iso_country")
    private String code;
    @Column(name = "name")
    private String name;
    @Column(name = "continent")
    private String continent;
    @Column(name = "wikipedia_link")
    private String wikipediaLink;
    @Column(name = "keywords")
    private String keywords;

    @OneToMany(mappedBy ="country", fetch = FetchType.LAZY)
    @JsonManagedReference
    List<Airport> airports;



    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContinent() {
        return continent;
    }

    public void setContinent(String continent) {
        this.continent = continent;
    }

    public String getWikipediaLink() {
        return wikipediaLink;
    }

    public void setWikipediaLink(String wikipediaLink) {
        this.wikipediaLink = wikipediaLink;
    }

    public String getKeywords() {
        return keywords;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    public Country id(final String id) {
        this.id = id;
        return this;
    }

    public Country code(final String code) {
        this.code = code;
        return this;
    }

    public Country name(final String name) {
        this.name = name;
        return this;
    }

    public Country continent(final String continent) {
        this.continent = continent;
        return this;
    }

    public Country wikipediaLink(final String wikipediaLink) {
        this.wikipediaLink = wikipediaLink;
        return this;
    }

    public Country keywords(final String keywords) {
        this.keywords = keywords;
        return this;
    }

    public List<Airport> getAirports() {
        return airports;
    }

    public void setAirports(List<Airport> airports) {
        this.airports = airports;
    }

    @Override
    public String toString() {
        return "Country{" +
                "id='" + id + '\'' +
                ", code='" + code + '\'' +
                ", name='" + name + '\'' +
                ", continent='" + continent + '\'' +
                ", wikipediaLink='" + wikipediaLink + '\'' +
                ", keywords='" + keywords + '\'' +
                '}';
    }
}
